import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Answer {

   public static void main (String[] param) {

      // TODO!!! Solutions to small problems
      //   that do not need an independent method!

      // conversion double -> String
      Double d = 1.2;
      String st = java.lang.String.valueOf(d);
      System.out.println(st);

      // conversion String -> int
      st = "1351335";
      try {
         Integer in = Integer.parseInt(st);
         System.out.println(in);
      } catch (Exception e) {
         System.out.println(e);
      }

      // "hh:mm:ss"
      DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm:ss");
      LocalDateTime time = LocalDateTime.now();
      System.out.println(format.format(time));

      // cos 45 deg
      System.out.println(Math.cos(Math.toRadians(45)));

      // table of square roots
      for (int i = 0; i < 101; i += 5) {
         System.out.println("int: " + i + " root: " + i * i);
      }

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string
      String stringToReverse = "Hello,321";
      StringBuilder r = new StringBuilder().append(stringToReverse);
      r.reverse();
      System.out.println(r);


      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // stop program
      long time1 = System.currentTimeMillis();
      try {
         Thread.sleep(3000);
      } catch (Exception e) {
         System.out.println(e);;
      }
      long time2 = System.currentTimeMillis();
      System.out.println(time2 - time1);

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (Integer.valueOf (generaator.nextInt(1000)));
      }

      // minimal element

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs
      HashMap<String, String> hashMap = new HashMap<>();
      hashMap.put("ICA0003", "Andmeturve ja krüptoloogia");
      hashMap.put("ICA0005", "Andmebaasisüsteemide alused ");
      hashMap.put("ICY0006", "Tõenäosusteooria ja matemaatiline statistika ");
      hashMap.put("ICD0001", "Algoritmid ja andmestruktuurid");
      hashMap.put("ICD0008", "Programmeerimine C# keeles ");

      for (String s1 : hashMap.keySet()) {
         System.out.println(s1);
      }
      hashMap.remove("ICA0003");
      for (String s1 : hashMap.keySet()) {
         System.out.println(s1 + " " + hashMap.get(s1));
      }


      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
   T maximum (Collection<? extends T> a)
           throws NoSuchElementException {

      Optional<? extends T> result = a.stream().max(Comparator.naturalOrder());
      return result.get();
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      String[] textSplit = text.split("\\s+");
      List<String> result = new ArrayList<>();

      for (String s : textSplit) {
         if (!s.isEmpty()) {
            result.add(s);
         }
      }
      return result.size();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      char[] sChars = s.toCharArray();
      StringBuilder result = new StringBuilder();

      for (char sChar : sChars) {
         if (Character.isUpperCase(sChar)) {
            result.append(String.valueOf(sChar).toLowerCase());
         } else if (Character.isLowerCase(sChar)) {
            result.append(String.valueOf(sChar).toUpperCase());
         } else {
            result.append(sChar);
         }
      }

      return result.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
           throws UnsupportedOperationException {
      Collections.reverse(list);
   }
}
